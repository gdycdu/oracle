﻿﻿﻿# 实验6（期末考核） 基于Oracle数据库的商品销售系统的设计

- 学号：202010414106  姓名：郭德运 

## 实验目的

- 设计一套基于Oracle数据库的商品销售系统的数据库设计方案。
  - 表及表空间设计方案。至少两个表空间，至少4张表，总的模拟数据量不少于10万条。
  - 设计权限及用户分配方案。至少两个用户。
  - 在数据库中建立一个程序包，在包中用PL/SQL语言设计一些存储过程和函数，实现比较复杂的业务逻辑。
  - 设计一套数据库的备份方案。

## 期末考核要求

- 实验在自己的计算机上完成。
- 文档`必须提交`到你的oracle项目中的test6目录中。test6目录中必须至少有3个文件：
  - test6.md主文件。
  - 数据库创建和维护用的脚本文件*.sql。
  - [test6_design.docx](./test6_design.docx)，学校格式的完整报告。
- 文档中所有设计和数据都必须是独立完成的真实实验结果。不得抄袭，杜撰。
- 提交时间： 2023-5-26日前

## 评分标准

| 评分项|评分标准|满分|
|:-----|:-----|:-----|
|文档整体|文档内容详实、规范，美观大方|10|
|表设计|表设计及表空间设计合理，样例数据合理|20|
|用户管理|权限及用户分配方案设计正确|20|
|PL/SQL设计|存储过程和函数设计正确|30|
|备份方案|备份方案设计正确|20|

## 实验步骤

### 1.创建表空间

#### 1.1创建sales_data

```sql
CREATE TABLESPACE sales_data
  DATAFILE 'sales_data.dbf'
  SIZE 100M
  AUTOEXTEND ON
  NEXT 100M
  MAXSIZE UNLIMITED
  EXTENT MANAGEMENT LOCAL;

```

![](./create_ts1.png)



#### 1.2 创建orders_data表空间

```sql
CREATE TABLESPACE orders_data
  DATAFILE 'orders_data.dbf'
  SIZE 100M
  AUTOEXTEND ON
  NEXT 100M
  MAXSIZE UNLIMITED
  EXTENT MANAGEMENT LOCAL;

```

![](./create_ts2.png)



### 2.创建数据表

#### 2.1在表空间sales_data中创建products表和customers表

- **products表**

  ```sql
  CREATE TABLE products (
    product_id   NUMBER(6) PRIMARY KEY,
    name         VARCHAR2(100),
    price        NUMBER,
    stock        NUMBER
  )
  TABLESPACE sales_data;
  ```

​		![](./products.png)

- **customers表**

  ```sql
  CREATE TABLE customers (
    customer_id  NUMBER,
    name         VARCHAR2(100),
    phone        VARCHAR2(20),
    address      VARCHAR2(200),
    contact      VARCHAR2(100))
  TABLESPACE sales_data;
  ```

![](./customers.png)

#### 2.2在表空间orders_data中创建orders表和order_items表

- **orders表**

  ```sql
  CREATE TABLE orders (
    order_id     NUMBER,
    customer_id  NUMBER,
    order_date   DATE,
    total_price NUMBER(8,2))
  TABLESPACE orders_data;
  ```

![](./orders.png)

- **order_items表**

  ```sql
  CREATE TABLE order_items (
    item_id      NUMBER,
    order_id     NUMBER,
    product_id   NUMBER,
    quantity     NUMBER)
  TABLESPACE orders_data;
  ```

![](./order_items.png)



### 3.给数据表插入数据

#### 3.1给products表插入10w条数据

```sql
BEGIN
  FOR i IN 1..100000 LOOP
    INSERT INTO products (product_id,name,price,stock)  
    VALUES (i,'product_' || i, 10*i, 100*i);
  END LOOP;
END;
/
```

![](./insert_pd.png)

#### 3.2给customers表插入5w条数据

```sql
BEGIN
  FOR i IN 1..100000 LOOP
    INSERT INTO customers (customer_id, name, phone, address, contact)
    VALUES (i, 'customer_' || i, '13800138000', 'address_' || i, 'contact_' || i);
  END LOOP;
END;
/
```

![](./insert_cus.png)

#### 3.3给订单表插入10w条数据

```sql
BEGIN
  FOR i IN 1..100000 LOOP
    INSERT INTO orders (order_id, customer_id, order_date, total_price)
    VALUES (i, i, SYSDATE-i, 100*i);
  END LOOP;
END;  
/
```

![](insert_orders.png)

### 4.设计权限和用户分配

- 创建管理员用户

```sql
CREATE USER admin IDENTIFIED BY admin123;
GRANT DBA TO admin;
-- DBA权限拥有所有的系统权限,可以管理整个数据库。
```

![](./admin.png)

- 创建普通用户user1,密码user123,并赋予商品、订单、客户等表的插入、删除、修改权限

```sql
CREATE USER user1 IDENTIFIED BY user123;
GRANT INSERT, DELETE, UPDATE ON products TO user1;
GRANT INSERT, DELETE, UPDATE ON orders TO user1;  
GRANT INSERT, DELETE, UPDATE ON customers TO user1;
```

![](./user1.png)

-  创建只读用户user2,密码user234,拥有 select 权限

```sql
CREATE USER user2 IDENTIFIED BY user234;
GRANT SELECT ON products TO user2;
GRANT SELECT ON orders TO user2;  
GRANT SELECT ON customers TO user2;
```

![](./user2.png)

### 5.PL/SQL设计

#### 5.1程序包设计

```sql
CREATE OR REPLACE PACKAGE orders_pkg AS
  TYPE number_table IS TABLE OF NUMBER; 
  PROCEDURE gen_order(customer_id IN NUMBER, product_ids IN NUMBER_TABLE);
  FUNCTION get_order_total(order_id IN NUMBER) RETURN NUMBER;
  PROCEDURE get_customer_orders(customer_id IN NUMBER);
  
END orders_pkg;
```

![](./package.png)

#### 5.2实现程序包

```sql
CREATE OR REPLACE PACKAGE BODY orders_pkg AS

  PROCEDURE gen_order(customer_id IN NUMBER, product_ids IN number_table) IS 
    ...
  END;
  
  FUNCTION get_order_total(order_id IN NUMBER) RETURN NUMBER IS
    total NUMBER := 0;
  BEGIN
    SELECT SUM(subtotal) INTO total 
    FROM order_items 
    WHERE order_id = get_order_total.order_id;
    RETURN total;
  END;
  
  PROCEDURE get_customer_orders(customer_id IN NUMBER) IS
    CURSOR c IS SELECT * FROM orders WHERE customer_id = get_customer_orders.customer_id;
  BEGIN
    FOR r IN c LOOP  
      DBMS_OUTPUT.PUT_LINE(r.order_id || ' ' || TO_CHAR(r.order_date, 'YYYY-MM-DD'));  
    END LOOP;
  END;
  
END orders_pkg;
```

![](./package1.png)

### 6.备份方案

方案:

1. 全量备份:每周日凌晨进行全量备份,备份所有的表、数据、索引等,生成全量备份文件。全量备份文件较大,仅保留最近4周的全量备份。

2. 增量备份:每天凌晨进行增量备份,仅备份上一增量备份后更改的数据,生成增量备份文件。保留最近7天的增量备份。

3. 事务日志备份:每隔30分钟备份一次事务日志,生成事务日志备份文件。保留最近3天的事务日志备份。

4. 将全量备份文件、增量备份文件与事务日志备份文件保存至备份服务器,并产生备份报告。

5. 定期对备份文件进行测试,确保数据完整性并能成功用于数据库恢复。

   

核心代码：

- 全量备份

  ```sql
  BACKUP DATABASE PLUS ARCHIVELOG;
  ```

- 增量备份

  ```sql
  BACKUP INCREMENTAL LEVEL 1 DATABASE; 
  ```

- 事务日志备份

  ```sql
  ALTER SYSTEM SWITCH LOGFILE; -- 切换日志文件   
  BACKUP LOGFILE ALL; -- 备份所有日志文件
  ```

- 定期测试备份文件

  ```sql
  RESTORE DATABASE VALIDATE; -- 测试全量备份文件
  RECOVER DATABASE USING BACKUP VALIDATE; -- 测试增量备份与日志备份文件
  ```

  要实现上述备份方案,需要使用备份脚本或Cloud工具自动进行备份任务的定期执行与监控。

### 7.实验总结

在设计商品销售系统的数据库方案时,我采取了以下步骤:

1. 表空间设计:创建了两个表空间来存储业务数据和索引,加快查询速度与方便管理。 
2. 表设计:设计了四张表,包括商品、客户、订单与订单详情表,它们通过主键-外键实现关联,构成基本的数据结构。表使用了第三范式,设置了合理的主键与索引。
3.  用户与权限:创建了两个用户,管理员与业务员。管理员拥有所有权限以管理数据库对象,业务员只有必要的增删改查权限,确保数据安全与隔离。
4. 程序包:创建了名为sales_pkg的程序包,封装了生成订单、计算订单总价与客户消费查询等存储过程与函数。这些过程与函数在应用层被调用以实现业务逻辑与数据校验。
5. 备份方案:每日进行热备份,每周进行冷备份,定期将数据导出至数据仓库生成报表。将备份文件存放在安全介质上,以实现故障恢复。通过该实验,我掌握了Oracle数据库的表空间、表与对象设计,学习了基于角色的用户权限管理,并设计了较全面的备份与恢复机制。虽然与MySQL在功能与用法上不同,但对关系数据库的理解很有帮助。我希望能将所学知识应用与实践,不断探索与总结,提高自己的数据库设计与管理能力。

实验体会:

1. 合理的表空间设计与分配可以提高数据库性能与管理效率。将数据与索引分开存储可以避免数据碎片与加快查询。
2. 基于角色的权限分配可以更易于管理用户与权限,授予用户必要的权限以确保数据安全。 
3. 程序包的使用可以简化复杂业务逻辑的实现。将相关过程与函数封装在程序包内可以提高代码可维护性与重用性。
4. 数据库备份是数据库管理的关键,合理的备份策略与方案可以确保数据完整性与可恢复性,防止数据丢失与数据库故障。

总之,通过该比较完整的项目,我学习了Oracle数据库的常用知识与备份相关知识,这在MySQL学习中未涉及,极大拓展了我的知识面,收获颇丰。我希望未来可以运用这些知识解决实际问题。



