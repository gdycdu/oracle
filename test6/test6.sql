CREATE TABLESPACE sales_data DATAFILE 'sales_data.dbf' SIZE 100M AUTOEXTEND ON NEXT 100M MAXSIZE UNLIMITED EXTENT MANAGEMENT LOCAL;
CREATE TABLESPACE orders_data DATAFILE 'orders_data.dbf' SIZE 100M AUTOEXTEND ON NEXT 100M MAXSIZE UNLIMITED EXTENT MANAGEMENT LOCAL;
CREATE TABLE products ( product_id NUMBER ( 6 ) PRIMARY KEY, NAME VARCHAR2 ( 100 ), price NUMBER, stock NUMBER ) TABLESPACE sales_data;
CREATE TABLE customers (
	customer_id NUMBER,
	NAME VARCHAR2 ( 100 ),
	phone VARCHAR2 ( 20 ),
	address VARCHAR2 ( 200 ),
contact VARCHAR2 ( 100 )) TABLESPACE sales_data;
CREATE TABLE orders (
	order_id NUMBER,
	customer_id NUMBER,
	order_date DATE,
total_price NUMBER ( 8, 2 )) TABLESPACE orders_data;
CREATE TABLE order_items ( item_id NUMBER, order_id NUMBER, product_id NUMBER, quantity NUMBER ) TABLESPACE orders_data;
BEGIN
		FOR i IN 1..100000
	LOOP
			INSERT INTO products ( product_id, NAME, price, stock )
		VALUES
			( i, 'product_' || i, 10 * i, 100 * i );
		
	END LOOP;
	
END;
/ BEGIN
	FOR i IN 1..100000
	LOOP
			INSERT INTO customers ( customer_id, NAME, phone, address, contact )
		VALUES
			( i, 'customer_' || i, '13800138000', 'address_' || i, 'contact_' || i );
		
	END LOOP;
	
END;
/ BEGIN
	FOR i IN 1..100000
	LOOP
			INSERT INTO orders ( order_id, customer_id, order_date, total_price )
		VALUES
			( i, i, SYSDATE - i, 100 * i );
		
	END LOOP;
	
END;
/ CREATE USER admin IDENTIFIED BY admin123;
GRANT DBA TO admin;-- DBA权限拥有所有的系统权限,可以管理整个数据库。
CREATE USER user1 IDENTIFIED BY user123;
GRANT INSERT,
DELETE,
UPDATE ON products TO user1;
GRANT INSERT,
DELETE,
UPDATE ON orders TO user1;
GRANT INSERT,
DELETE,
UPDATE ON customers TO user1;
CREATE USER user2 IDENTIFIED BY user234;
GRANT SELECT ON
	products TO user2;
GRANT SELECT ON
	orders TO user2;
GRANT SELECT ON
	customers TO user2;
CREATE 
	OR REPLACE PACKAGE orders_pkg AS TYPE number_table IS TABLE OF NUMBER;
PROCEDURE gen_order ( customer_id IN NUMBER, product_ids IN NUMBER_TABLE );
FUNCTION get_order_total ( order_id IN NUMBER ) RETURN NUMBER;
PROCEDURE get_customer_orders ( customer_id IN NUMBER );

END orders_pkg;
CREATE 
	OR REPLACE PACKAGE BODY orders_pkg AS PROCEDURE gen_order ( customer_id IN NUMBER, product_ids IN number_table ) IS...
	END;
FUNCTION get_order_total ( order_id IN NUMBER ) RETURN NUMBER IS total NUMBER := 0;
BEGIN
	SELECT
		SUM( subtotal ) INTO total 
	FROM
		order_items 
	WHERE
		order_id = get_order_total.order_id;
	RETURN total;
	
END;
PROCEDURE get_customer_orders ( customer_id IN NUMBER ) IS CURSOR c IS SELECT
* 
FROM
	orders 
WHERE
	customer_id = get_customer_orders.customer_id;
BEGIN
		FOR r IN c
	LOOP
			DBMS_OUTPUT.PUT_LINE (
			r.order_id || ' ' || TO_CHAR ( r.order_date, 'YYYY-MM-DD' ));
		
	END LOOP;
	
END;

END orders_pkg;
address VARCHAR2 ( 200 ),
contact VARCHAR2 ( 100 )) TABLESPACE sales_data;