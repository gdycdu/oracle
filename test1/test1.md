# 实验1：SQL语句的执行计划分析与优化指导

- 学号：202010414106
- 姓名：郭德运
- 班级：1

## 实验目的

  分析SQL执行计划，执行SQL语句的优化指导。理解分析SQL语句的执行计划的重要作用。

## 实验数据库和用户

  数据库是pdb，用户是sys和hr

## 实验内容

对Oracle12c中的HR人力资源管理系统中的表进行查询与分析。设计自己的查询语句，并作相应的分析，查询语句不能太简单。执行两个比较复杂的返回相同查询结果数据集的SQL语句，通过分析SQL语句各自的执行计划，判断哪个SQL语句是最优的。最后将你认为最优的SQL语句通过sqldeveloper的优化指导工具进行优化指导，看看该工具有没有给出优化建议。



### 实验步奏

普通用户不允许查询执行计划，必须有 `plustrace` 角色才可以。Oracle 的插接式数据库 本身并没有默认创建 `plustrace` 角色，所以需要首先在 `pdb` 数据库中创建角色 `plustrace`， 方法是用 `sys` 登录到 `PDB` 数据库，然后运行`$ORACLE_HOME/sqlplus/admin/plustrce.sql` 脚本文件，最后通过“`GRANT plustrace to 用户名;`”命令将 `plustrace` 赋予用户。

~~~sql
sqlplus sys/123 as sysdba
SQL> @$ORACLE_HOME/sqlplus/admin/plustrce.sql
SQL> 
SQL> drop role plustrace;

Role dropped.

SQL> create role plustrace;

Role created.

SQL> 
SQL> grant select on v_$sesstat to plustrace;

Grant succeeded.

SQL> grant select on v_$statname to plustrace;

Grant succeeded.

SQL> grant select on v_$mystat to plustrace;

Grant succeeded.

SQL> grant plustrace to dba with admin option;

Grant succeeded.

SQL> 
SQL> set echo off
SQL> GRANT plustrace TO hr;

Grant succeeded.

SQL> GRANT SELECT ON v_$sql TO hr;

Grant succeeded.

SQL> GRANT SELECT ON v_$sql_plan TO hr;

Grant succeeded.

SQL> GRANT SELECT ON v_$sql_plan_statistics_all TO hr;

Grant succeeded.

SQL> GRANT SELECT ON v_$session TO hr;

Grant succeeded.

SQL> GRANT SELECT ON v_$parameter TO hr;

Grant succeeded.

~~~

#### 查询语句

这里我们执行查询两个部门（'IT'和‘Sale’）的部门总人数和平均工资

##### 查询1

~~~sql
SELECT d.department_name，count(e.job_id)as "部门总人数"，
avg(e.salary)as "平均工资"
from hr.departments d，hr.employees e
where d.department_id = e.department_id
and d.department_name in ('IT'，'Sales')
GROUP BY department_name;
~~~

**执行计划**

~~~sql
Execution Plan
----------------------------------------------------------
Plan hash value: 3808327043

--------------------------------------------------------------------------------
-------------------

| Id  | Operation| Name		  | Rows  | Bytes | Cost (%CPU)| Time|

--------------------------------------------------------------------------------
-------------------

|   0 | SELECT STATEMENT	|  |	1 |    23 |		5  (20)| 00:00:01 |

|   1 |  HASH GROUP BY		|  |	1 |    23 |		5  (20)| 00:00:01 |

|   2 |  NESTED LOOPS		|  |   19 |   437 |		4   (0)| 00:00:01 |

|   3 |  NESTED LOOPS 	|   |   20 |   437 |		4   (0)| 00:00:01 |

|*  4 |  TABLE ACCESS FULL	| DEPARTMENTS|	2 | 32 |3   (0)| 00:00:01 |

|*  5 |  INDEX RANGE SCAN	| EMP_DEPARTMENT_IX | 10 ||0   (0)| 00:00:01 |

|   6 |  TABLE ACCESS BY INDEX ROWID| EMPLOYEES	  | 10 | 70 |1   (0)| 00:00:01 |

--------------------------------------------------------------------------------
-------------------


Predicate Information (identified by operation id):
---------------------------------------------------

   4 - filter("D"."DEPARTMENT_NAME"='IT' OR "D"."DEPARTMENT_NAME"='Sales')
   5 - access("D"."DEPARTMENT_ID"="E"."DEPARTMENT_ID")

Note
-----
   - this is an adaptive plan

~~~

**统计信息**

~~~sql
Statistics
----------------------------------------------------------
	  0  recursive calls
	  0  db block gets
	  9  consistent gets
	  0  physical reads
	  0  redo size
	813  bytes sent via SQL*Net to client
	602  bytes received via SQL*Net from client
	  2  SQL*Net roundtrips to/from client
	  0  sorts (memory)
	  0  sorts (disk)
	  2  rows processed

~~~

这里我们可以看到用时为**0.003**s，属于是非常快的速度了。



##### 查询2

~~~sql
SELECT d.department_name，count(e.job_id)as "部门总人数"，
avg(e.salary)as "平均工资"
FROM hr.departments d，hr.employees e
WHERE d.department_id = e.department_id
GROUP BY department_name
HAVING d.department_name in ('IT'，'Sales');
~~~

**执行计划**

~~~sql
Execution Plan
----------------------------------------------------------
Plan hash value: 2128232041

--------------------------------------------------------------------------------
--------------

| Id  | Operation		       | Name	     | Rows  | Bytes | Cost (%CP
U)| Time     |

--------------------------------------------------------------------------------
--------------

|   0 | SELECT STATEMENT	       |	     |	   1 |	  23 |	   7  (2
9)| 00:00:01 |

|*  1 |  FILTER 		       |	     |	     |	     |
  |	     |

|   2 |   HASH GROUP BY 	       |	     |	   1 |	  23 |	   7  (2
9)| 00:00:01 |

|   3 |    MERGE JOIN		       |	     |	 106 |	2438 |	   6  (1
7)| 00:00:01 |

|   4 |     TABLE ACCESS BY INDEX ROWID| DEPARTMENTS |	  27 |	 432 |	   2   (
0)| 00:00:01 |

|   5 |      INDEX FULL SCAN	       | DEPT_ID_PK  |	  27 |	     |	   1   (
0)| 00:00:01 |

|*  6 |     SORT JOIN		       |	     |	 107 |	 749 |	   4  (2
5)| 00:00:01 |

|   7 |      TABLE ACCESS FULL	       | EMPLOYEES   |	 107 |	 749 |	   3   (
0)| 00:00:01 |

--------------------------------------------------------------------------------
--------------


Predicate Information (identified by operation id):
---------------------------------------------------

   1 - filter("DEPARTMENT_NAME"='IT' OR "DEPARTMENT_NAME"='Sales')
   6 - access("D"."DEPARTMENT_ID"="E"."DEPARTMENT_ID")
       filter("D"."DEPARTMENT_ID"="E"."DEPARTMENT_ID")

~~~

**统计信息**

~~~sql
Statistics
----------------------------------------------------------
	  8  recursive calls
	  0  db block gets
	 10  consistent gets
	  0  physical reads
	  0  redo size
	813  bytes sent via SQL*Net to client
	828  bytes received via SQL*Net from client
	  2  SQL*Net roundtrips to/from client
	  1  sorts (memory)
	  0  sorts (disk)
	  2  rows processed
~~~

##### 择优

两句查询语句的`physical reads`都为0，因此去看另外一个参数`consistent gets`，根据`physical reads`、`db block gets`和 `consistent  gets` 这三个参数的换算公式：
$$
数据缓冲区的使用命中率 = 1-(physical\quad reads)/(db\quad block\quad gets+consistent\quad gets)
$$
$consistent\quad gets_1 < consistent\quad gets_2$

因此第二句话的数据缓冲区命中率要大一些，因此执行效率要高一些，事实上也是的，第二句话的执行时间明显是比第一句话快的。

##### 优化建议

可见第二句查询语句已经做到简洁，至少`sqldeveloper`并没有给出优化建议。

而对于第一条语句，`sqldeveloper`却给出了优化建议，说明我们认为第二句查询语句的执行效率优于第一句是正确的。

至此，实验一结束。



## 总结

通过本次实验确实学到了很多东西，我是自己配置的环境，因此中途其实出现过各种各样的情况，只能参考网上的各种资料进行修改和配置，但好歹是磕磕绊绊的完成了本次实验，了解了12C中容器的概念，以及如何使用`sqldeveloper`去优化`sql`语句，评估`sql`语句。学习的经历总是深刻的，我通过本次实验才了解`CDB`与`PDB`的关系，我才知道原来我一直在`CDB`下活动，之后会转到`pdb`下活动，`cdb`中存在诸多限制，非常不方便。对于用户的授权也有了一定的理解，并且在使用`sqldeveloper`执行优化指导时，要求`HR`用户还需拥有`advisor`权限，因此还需要手动赋予，这同样加深了对`Oracle`数据库中角色用户的理解。
